import React, { Component } from 'react';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            val: '',
        };
    }

    render() {
        return (
            <div>
                <button onClick={() => alert('boom!')}>
                    React Button
                </button>

                <hr />

                <label htmlFor="inp">Data binding:</label>
                <input
                    id="inp"
                    type="text"
                    placeholder="type a word..."
                    onChange={e => this.setState({ val: e.target.value })}
                />
                <span style={{marginLeft: 10}}>{this.state.val}</span>
            </div>
        );
    }
}

export default App;